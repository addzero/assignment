type Person interface {
	Create(entities.Person) error
	List() ([]entities.Person, error)
}