type PersonList interface {
	Call() (entities.Person, error)
}