package main

import (
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	validator "github.com/go-playground/validator/v10"
	env "github.com/joho/godotenv"
)

const envFile = ".env"
const dataFile = "data/forms.json"

var loadEnv = env.Load

type formInput struct {
	FirstName   string `json:"first_name" validate:"required"`
	LastName    string `json:"last_name" validate:"required"`
	Email       string `json:"email" validate:"required,email"`
	PhoneNumber string `json:"phone_number" validate:"required,numeric,startswith=0"`
}

type Person struct {
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phone_number"`
}

func (f formInput) validate() error {
	validate := validator.New()
	if err := validate.Struct(f); err != nil {
		return err
	}
	return nil
}

func (f formInput) save() error {
	file, err := ioutil.ReadFile(dataFile)
	if err != nil {
		return err
	}
	var forms []formInput
	err = json.Unmarshal(file, &forms)
	if err != nil {
		return err
	}

	forms = append(forms, f)
	toSave, err := json.Marshal(forms)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(dataFile, toSave, os.ModeAppend)
	return err
}

func handlePerson(resp http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		persons := []Person{}
		file, err := ioutil.ReadFile(dataFile)
		if err != nil {
			return
		}
		err = json.Unmarshal(file, &persons)
		if err != nil {
			return
		}

		tmpl := template.Must(template.ParseFiles("persons.html"))
		tmpl.Execute(resp, persons)
		return
	}
}

func handleFunc(resp http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodPost:

		err := req.ParseForm()
		// err := json.NewDecoder(req.Body).Decode(&f)
		if err != nil {
			resp.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(resp, err.Error())
			return
		}

		f := formInput{
			FirstName:   req.FormValue("first_name"),
			LastName:    req.FormValue("last_name"),
			Email:       req.FormValue("email"),
			PhoneNumber: req.FormValue("phone_number"),
		}

		err = f.validate()
		if err != nil {
			resp.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(resp, err.Error())
			return
		}
		err = f.save()
		if err != nil {
			resp.WriteHeader(http.StatusInternalServerError)
			fmt.Fprint(resp, err.Error())
			return
		}
		// resp.WriteHeader(http.StatusOK)
		// fmt.Fprint(resp, "form saved")
		http.Redirect(resp, req, "http://localhost:8080/person", http.StatusSeeOther)
	case http.MethodGet:
		tmpl := template.Must(template.ParseFiles("form.html"))
		tmpl.Execute(resp, nil)
		return
	default:
		log.Println("error no 404")
		resp.WriteHeader(http.StatusNotFound)
		fmt.Fprint(resp, "not found")
	}
}

func run() (s *http.Server) {
	err := loadEnv(envFile)
	if err != nil {
		log.Fatal(err)
	}
	port, exist := os.LookupEnv("PORT")
	if !exist {
		log.Fatal("no port specified")
	}
	port = fmt.Sprintf(":%s", port)

	mux := http.NewServeMux()
	mux.HandleFunc("/", handleFunc)
	mux.HandleFunc("/person", handlePerson)

	s = &http.Server{
		Addr:           port,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
		Handler:        mux,
	}

	go func() {
		if err := s.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	return
}

func main() {
	s := run()
	quit := make(chan os.Signal)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutting down server...")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()
	if err := s.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown")
	}
	log.Println("Server exiting")
}
