module hornestbank

go 1.13

require (
	github.com/go-playground/validator/v10 v10.4.1 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83 // indirect
	golang.org/x/sys v0.0.0-20210228012217-479acdf4ea46 // indirect
)
